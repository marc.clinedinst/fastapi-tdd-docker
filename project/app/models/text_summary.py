from tortoise import fields, models
from tortoise.contrib.pydantic import pydantic_model_creator


class TextSummary(models.Model):
    created_at = fields.DatetimeField(auto_now_add=True)
    summary = fields.TextField()
    url = fields.TextField()

    def __str__(self):
        return self.url


SummarySchema = pydantic_model_creator(TextSummary)
