from app.api import crud
from app.models.schemas import SummaryPayloadSchema, SummaryResponseSchema
from app.models.text_summary import SummarySchema
from fastapi import APIRouter, HTTPException
from typing import List

router = APIRouter()


@router.get("/", response_model=List[SummarySchema])
async def read_all_summaries() -> List[SummarySchema]:
    return await crud.get_all()


@router.post("/", response_model=SummaryResponseSchema, status_code=201)
async def create_summary(payload: SummaryPayloadSchema) -> SummaryResponseSchema:
    summary_id = await crud.post(payload)
    response_object = {"id": summary_id, "url": payload.url}

    return response_object


@router.get("/{_id}/", response_model=SummarySchema)
async def read_summary(_id: int) -> SummarySchema:
    summary = await crud.get(_id)

    if not summary:
        raise HTTPException(status_code=404, detail="Summary not found.")

    return summary
