from app.config import get_settings, Settings
from fastapi import APIRouter, Depends

router = APIRouter()


@router.get("/")
async def root(settings: Settings = Depends(get_settings)):
    return {"message": "Hello, World!"}
