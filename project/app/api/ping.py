from app.config import get_settings, Settings
from fastapi import APIRouter, Depends

router = APIRouter()


@router.get("/ping")
async def pong(settings: Settings = Depends(get_settings)):
    return {
        "database": settings.database_url,
        "environment": settings.environment,
        "ping": "pong!",
        "testing": settings.testing,
    }
