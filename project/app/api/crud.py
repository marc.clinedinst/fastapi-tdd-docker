from app.models.schemas import SummaryPayloadSchema
from app.models.text_summary import TextSummary
from typing import List, Union


async def get(id: int) -> Union[dict, None]:
    summary = await TextSummary.filter(id=id).first().values()

    if summary:
        return summary[0]

    return None


async def get_all() -> List:
    summaries = await TextSummary.all().values()

    return summaries


async def post(payload: SummaryPayloadSchema) -> int:
    summary = TextSummary(summary="dummy summary", url=payload.url)
    await summary.save()

    return summary.id
