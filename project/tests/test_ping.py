import os


def test_ping(test_app):
    response = test_app.get("/ping")

    assert response.status_code == 200
    assert response.json() == {
        "database": os.getenv("DATABASE_TEST_URL"),
        "environment": "dev",
        "ping": "pong!",
        "testing": True,
    }
