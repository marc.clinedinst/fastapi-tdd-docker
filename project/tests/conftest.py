import os
import pytest

from app.config import get_settings, Settings
from app.main import create_application
from starlette.testclient import TestClient
from tortoise.contrib.fastapi import register_tortoise


def get_settings_override():
    return Settings(database_url=os.environ.get("DATABASE_TEST_URL"), testing=1)


@pytest.fixture(scope="module")
def test_app():
    app = create_application()
    app.dependency_overrides[get_settings] = get_settings_override

    with TestClient(app) as test_client:
        yield test_client


@pytest.fixture(scope="module")
def test_app_with_db():
    app = create_application()
    app.dependency_overrides[get_settings] = get_settings_override
    register_tortoise(
        app,
        add_exception_handlers=True,
        db_url=os.getenv("DATABASE_TEST_URL"),
        generate_schemas=True,
        modules={"models": ["app.models.text_summary"]},
    )

    with TestClient(app) as test_client:
        yield test_client
